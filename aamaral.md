# Questionário

### Qual seu nome?
André Felipe Amaral Siqueira

### Qual sua idade?
32 anos

### Qual sua área de formação? (não necessarimente acadêmica):
Somente **ensino médio**. Cheguei a fazer metade do curso de *Licenciatura em Física*, mas não tive como terminar por falta de grana.

### Qual sua área de atuação?
Servidor estadual da justiça. No entanto, minha é vocação definitivamente é em *tecnologia da informação*, tanto que considero minha melhor experiência profissional os 3 anos que servi na Camara Municipal de Luziania-GO como *Técnico de Informática*.

### A Quanto tempo usa GNU/Linux?
Aproximadamente **quatro** anos.

### Adminstra ou administrou servidores GNU/Linux?
Somente um **servidor de arquivos usando Debian + SAMBA** em uma rede com estações Windows. Mas tinha planos de trocar o servidor Mikrotik ou para um *pfSense* ou para um *Debian + ferramentas para gerenciamento da rede*. Acabou que fui chamado para esse cargo na justiça e não tive tempo hábil para fazer essa mudança.

### A quanto tempo usa Debian?
Aproximadamente quatro anos. Comecei com *Ubuntu* mas fiquei nele por pouco tempo.

### Qual sua distribuição preferida?
**Debian**, por ser uma das poucas que conheço que deixam claros e bem escritos os seus valores e que **se importam com os valores do SL**. O que me incomoda atualmente é a adoção do *systemd* como *init* padrão.

### Em seu computador pessoal qual sistema e distribuição principal?
Atualmente o **Void**, justamente pela questão do *init system*.

### Qual seu editor de texto puro preferido?
**VIM**.

### É programador(a)? Que linguagens?
Não, só que me aventuro bastante em *shellscript*, *free-pascal (usando Lazarus)* e *SQL*.

### Tem experiência em compilar programas?
No máximo um `gcc -Wall hello.c -o hello` heheh.

### Já criou pacotes deb?
Não.

### Já criou pacotes de alguma outra distribuição?
Não.

