# Construção da Jaula Sid
Obs. baseado no Guia de Empacotamento debian 3.5 de João Eriberto Mota Filho.

** crie um par de chaves no seu sistema

## Instalação básica

- criar um diretório para jaula.
  (partição, disco-virtual, etc)

- montar (se for o caso)

- instalar o sistema básico

```
debootstrap sid jaula-sid http://deb.debian.org/debian
```

- chrootar (enjaular) depois vamos usar um script

```
mount --bind /proc jaula-sid/proc
chroot jaula-sid
apt update
```

## Completando instalação
```
apt install autopkgtest blhc devscripts dh-make dput-ng git-buildpackage how-can-i-help vim quilt spell tardiff tree bash-completion
```

## Configurando timezone
```
dpkg-reconfigure tzdata
```

## Ajustes

** Importe as chaves

- /etc/bash.bashrc

No final do arquivo /etc/bash.bashrc :

```
alias ls='ls --color=auto'
alias tree='tree -aC'
alias debuildsa='dpkg-buildpackage -sa -ksua_chave_gpg'
alias uscan-check='uscan --verbose --report'
alias debcheckout='debcheckout -a'
export DEBFULLNAME="seu_nome_completo_sem_acentos/cedilha"
export DEBEMAIL="seu_e-mail"
export EDITOR=vim
export LANG=C.UTF-8
export LANGUAGE=C.UTF-8
export LC_ALL=C.UTF-8
export QUILT_PATCHES=debian/patches
#export PS1='JAULA-SID-\u@\h:\w\$ '
export PS1="\[\033[1;35m\]\342\224\214\342\224\200\[\033[1;37m\][\[\033[1;33m\]Jaula-Sid\[\033[1;37m\]]\[\033[1;35m\]\342\224\200\[\033[1;37m\][\[\033[1;32m\]\w\[\033[1;37m\]]\[\033[1;33m\](\[\033[1;37m\]\t\[\033[1;33m\])\n\[\033[1;35m\]\342\224\224\342\224\200\342\224\200\342\225\274\[\033[1;31m\] $ \[\033[0m\]"

```

Lendo bash.bashrc
```
source /etc/bash.bashrc
```

## Configurando lintian

Criar arquivo ~/.lintianrc ( /root/.lintianrc )
com as seguintes linhas:

```
display-info = yes
pedantic = yes
display-experimental = yes
color = auto
```

## Ajuste do /etc/apt/sources.list

```
deb http://deb.debian.org/debian sid main
deb-src http://deb.debian.org/debian sid main
```

## Script para enjaular

```
#!/usr/bin/env bash
cd /vms/

mount -o bind /proc jaula-sid/proc
mount -o bind /sys jaula-sid/sys
mount -o bind /dev jaula-sid/dev
mount -o bind /dev/pts jaula-sid/dev/pts

chroot jaula-sid

umount -l jaula-sid/dev
umount jaula-sid/sys
umount jaula-sid/proc

```

## Criando chave gpg

Na sua máquina de trabalho:
```
gpg --gen-key --default-new-key-algo=rsa4096/cert,sign+rsa4096/encr
```

Enviando para um servidor de chaves (importante se for colaborar com o Debian)
(Não é obrigatório)

```
gpg --keyserver pool.sks-keyservers.net --send-key ID-DA-SUA-CHAVE
```

Exportando as chaves:

```
gpg -a --export nr_da_chave > chave.pub
gpg -a --export-secret-keys nr_da_chave > chave.key
```

Mova os arquivos para dentro da jaula unstable, faça o chroot, importe as chaves e remova os arquivos.

## Para importar:
```
gpg --list-keys
echo "pinentry-mode loopback" >> ~/.gnupg/gpg.conf
gpg --import chave.key chave.pub
gpg --list-keys
```

Habilitando a chave para a assinatura de pacotes.
Edite o arquivo /etc/devscripts.conf e insira o número COMPLETO
da sua chave GPG na linha DEBSIGN_KEYID, descomentando-a.

Veja exemplo:
``
DEBSIGN_KEYID=457ECB10EEC95A01AEBA1F0D2DE63B9C704EBE9EF
``

