﻿Curso de empacotamento Debian

Crie um arquivo s.md respondendo as seguintes perguntas:

Questionário para os alunos(as)

**- Qual seu nome?**
Josenilson Ferreira da Silva

**- Qual sua idade?**
47

**- Qual sua Área de formação?**
Pós Graduado em História:

**- Qual sua Área de atuação?**
Venda de Automóveis
(setor administrativo)

**- A Quanto tempo usa GNU/Linux?**
Uns dois Anos, porem com paradas.

**- Administra ou administrou servidores GNU/Linux?**
não

**- A quanto tempo usa Debian?**
6 meses

**- Qual sua distribuição preferida?**
Agora debian, antes Kali.

**- Em seu computador pessoal qual sistema e distribuição  principal?
Hoje, Debian /Ubuntu e Kali**

**- Qual seu editor de texto puro preferido?**
Gedit em modo gráfico, e o VIM em modo texto

**- A programador(a)? Que linguagens?**
não

**- Tem experiência em compilar programas?**
Pouca, mas tenho.

**- JÃ¡ criou pacotes deb?**
não

**- Já criou pacotes de alguma outra distribuição?**
não
