## Links para documentação

- Criando chave gpg
https://keyring.debian.org/creating-key.html

- Debian New Maintainers' Guide
https://www.debian.org/doc/manuals/maint-guide/

- Links do João Eriberto
https://people.debian.org/~eriberto/#links
http://eriberto.pro.br/wiki/index.php?title=FAQ_empacotamento_Debian

Vídeo sobre número de versões:
https://youtu.be/SEJs_VmdqtA

Slides:
http://eriberto.pro.br/files/videoaulas/debian/empacotamento_mini_aula_formatos_numeros_versao.pdf

- Debian Policy Manual
https://www.debian.org/doc/debian-policy/

- Referência para desenvolvedores
https://www.debian.org/doc/manuals/developers-reference/developers-reference.en.pdf

- Guia dos mantenedores
https://www.debian.org/doc/manuals/maint-guide/maint-guide.en.pdf

- Licenças
https://www.debian.org/legal/licenses/

- Manual de empacotamento
https://www.debian.org/doc/manuals/packaging-tutorial/packaging-tutorial.pt.pdf

- Lista de discussão
https://lists.debian.org/

- Sobre watch
https://wiki.debian.org/debian/watch

## Links do curso

- Tmate
https://tmate.io/t/kretcheu/debpkg
ssh kretcheu/debpkg@nyc1.tmate.io

- Aulas
https://jitsi.malamanhado.com.br/debpkg


